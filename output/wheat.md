## Intro

This is a replication of some results at [http://rawfoodsos.com/2010/09/02/the-china-study-wheat-and-heart-disease-oh-my/](http://rawfoodsos.com/2010/09/02/the-china-study-wheat-and-heart-disease-oh-my/).

## Wheat vs. heart disease in China Study 1.

Denise wrote "Correlation between wheat flour and coronary heart disease: 0.67".

My analysis says correlation is:

  * 0.4857 in China Study 1 data,
  * 0.6271, in China Study 2 data.


```r
p <- ggplot(merge(ch83m, ch83q), aes(x = dWHEAT, y = IHDc))
p <- p + geom_point() + geom_smooth(method = "lm")
p
```

```
## Warning: Removed 477 rows containing missing values (stat_smooth).
```

```
## Warning: Removed 477 rows containing missing values (geom_point).
```

![plot of chunk unnamed-chunk-1](../../../raw/master/output/figure/unnamed-chunk-1.png) 


Compare to [http://rawfoodsos.files.wordpress.com/2010/08/wheat_chd_1.jpg](http://rawfoodsos.files.wordpress.com/2010/08/wheat_chd_1.jpg). NOTE: looks different from Denise's graph.


## Wheat vs. heart disease in China Study 2.


```r
p <- ggplot(merge(ch89m, ch89q), aes(x = dWHEAT, y = IHDc))
p <- p + geom_point() + geom_smooth(method = "lm", formula = y ~ poly(x, 2))
p
```

```
## Warning: Removed 420 rows containing missing values (stat_smooth).
```

```
## Warning: Removed 420 rows containing missing values (geom_point).
```

![plot of chunk unnamed-chunk-2](../../../raw/master/output/figure/unnamed-chunk-2.png) 


Compare to [http://rawfoodsos.files.wordpress.com/2010/08/wheat_chd_2.jpg](http://rawfoodsos.files.wordpress.com/2010/08/wheat_chd_2.jpg).


## Negative correlates of reported wheat consumption.

  * All meat intake (r = -0.3502)
  * Red meat intake (r = -0.2987)
  * Animal fat intake (r = -0.3508)
  * Saturated fat intake (r = -0.4047)
  * Total animal protein intake (r = -0.3345)
  * Total fat intake (r = -0.4283)
  * Fat as a percentage of total calories (r = -0.4491)
  * Total cholesterol (r = -0.0906)
  * Apolipoprotein B (r = -0.0276)
  * Daily alcohol intake (r = -0.2198)


## Wheat vs. weight.


```r
p <- ggplot(merge(ch89dg, ch89q), aes(x = dWHEAT, y = dWEIGHT))
p <- p + geom_point() + geom_smooth(method = "lm")
p
```

```
## Warning: Removed 138 rows containing missing values (stat_smooth).
```

```
## Warning: Removed 138 rows containing missing values (geom_point).
```

![plot of chunk unnamed-chunk-3](../../../raw/master/output/figure/unnamed-chunk-3.png) 


Compare to [http://rawfoodsos.files.wordpress.com/2010/08/wheat_weight1.jpg](http://rawfoodsos.files.wordpress.com/2010/08/wheat_weight1.jpg).


## Wheat vs. BMI


```r
p <- ggplot(merge(ch89dg, ch89q), aes(x = dWHEAT, y = dBMI))
p <- p + geom_point() + geom_smooth(method = "lm")
p
```

```
## Warning: Removed 138 rows containing missing values (stat_smooth).
```

```
## Warning: Removed 138 rows containing missing values (geom_point).
```

![plot of chunk unnamed-chunk-4](../../../raw/master/output/figure/unnamed-chunk-4.png) 


Compare to [http://rawfoodsos.files.wordpress.com/2010/08/wheat_bmi1.jpg](http://rawfoodsos.files.wordpress.com/2010/08/wheat_bmi1.jpg).

