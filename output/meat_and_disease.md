## Intro

This is a replication of some results at [http://rawfoodsos.com/2010/06/01/a-closer-look-at-the-china-study-meat-and-disease/](http://rawfoodsos.com/2010/06/01/a-closer-look-at-the-china-study-meat-and-disease/). To facilitate comparison, I use Denise's headings and labels; as a result, a section may be named "Negative Correlations", even if some of the correlations I compute turn out to be positive.

## Meat intake in rural China

Meat intake range: 0, 94.6.
Meat intake average: 24.9406.

### Assorted Correlations with MEAT

  * Beer: 0.0231
  * Liquor: 0.293
  * Milk: 0.3369
  * Refined starch and sugar: 0.1404
  * Egg consumption: 0.1931
  * Use of snuff: -0.1759
  * Plant protein intake: -0.5562
  * Crude fiber intake: -0.5674
  * Other cereal grains intake: -0.4708
  * Starchy tubers intake: -0.309
  * Pipe smoking: -0.3393
  * Salted vegetable intake: -0.1639
  * Carrot intake: COULD NOT FIND VARIABLE
  * Sweet potato consumption: -0.1902

### NEGATIVE CORRELATIONS

  * Liver cirrhosis: -0.2663
  * Oesophageal cancer: -0.2324
  * Myocardial infarction and coronary heart disease: -0.2324
  * Cervix cancer: 

```

Error in cor(MEAT, CERVIXCAc, use = "complete") : 
  no complete element pairs

```


  * All cancers: -0.1414
  * Penis cancer: COULD NOT FIND VARIABLE
  * Diseases of blood and blood forming organs: -0.1279
  * Neurological diseases: -0.2841
  * Stroke: -0.2216
  * Diabetes: -0.2787
  * Lymphoma: 0.0911
  * All non-cancer causes: COULD NOT FIND VARIABLE
  * Stomach cancer: -0.2541
  * Hypertensive heart disease: 0.0144
  * Rheumatic heart disease: -0.1109
  * Leukemia: 0.1496

### POSITIVE CORRELATIONS

  * Colon cancer: COULD NOT FIND VARIABLE
  * Nasopharyngeal cancer: 0.4171
  * Breast cancer: 

```

Error in cor(MEAT, BREASTCAc, use = "complete") : 
  no complete element pairs

```


  * Colorectal cancer: 0.0687
  * Rectal cancer: COULD NOT FIND VARIABLE
  * Brain cancer: 0.2168

### SCHISTOSOMIASIS POSTITIVE CORRELATIONS

  * Rectal cancer: COULD NOT FIND VARIABLE
  * Colorectal cancer: 0.5517
  * Colon cancer: COULD NOT FIND VARIABLE
  * Brain cancer: 0.0851
