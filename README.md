This project is an open-source analysis of data from The China Study.

Results:

  * [Wheat and Heart Disease](https://bitbucket.org/masonicboom/chinastudy/src/master/output/wheat.md)
  * [Meat and Disease](https://bitbucket.org/masonicboom/chinastudy/src/master/output/meat_and_disease.md)


To reproduce analysis, on an OSX or Linux machine execute the command
"make", from within the directory containing "Makefile".

System Requirements:

  * [make](https://www.gnu.org/software/make/manual/make.html)
  * [curl](http://curl.haxx.se/)
  * [R](http://www.r-project.org/)

Background references:

  * [http://www.ctsu.ox.ac.uk/~china/monograph/](http://www.ctsu.ox.ac.uk/~china/monograph/)
  * [http://rawfoodsos.com/2010/07/07/the-china-study-fact-or-fallac/](http://rawfoodsos.com/2010/07/07/the-china-study-fact-or-fallac/)
  * [http://rawfoodsos.com/2010/09/02/the-china-study-wheat-and-heart-disease-oh-my/](http://rawfoodsos.com/2010/09/02/the-china-study-wheat-and-heart-disease-oh-my/)

Technical References:

  * [https://www.gnu.org/software/make/manual/make.html](https://www.gnu.org/software/make/manual/make.html)
  * [http://www.r-project.org/](http://www.r-project.org/)
  * [http://docs.ggplot2.org/current/](http://docs.ggplot2.org/current/)
  * [http://plyr.had.co.nz/](http://docs.ggplot2.org/current/)
