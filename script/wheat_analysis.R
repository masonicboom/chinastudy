library(ggplot2)
library(knitr)

# Prepare data.
load('ch83.Rdata')
load('ch89.Rdata')

# Place figures in output directory.
knitr::opts_knit$set(base.dir = normalizePath('output'))

# Set figure URLs so they render correctly on BitBucket.
knitr::opts_knit$set(base.url='../../../raw/master/output/')

# Render.
knit('report/wheat.Rmd', output='output/wheat.md')

