# Load data.
ch89m <- read.csv('data/CH89M.CSV', strip.white=T, na.strings=c('.'))
ch89dg <- read.csv('data/CH89DG.CSV', strip.white=T, na.strings=c('.'))
ch89q <- read.csv('data/CH89Q.CSV', strip.white=T, na.strings=c('.'))
ch89pru <- read.csv('data/CH89PRU.CSV', strip.white=T, na.strings=c('.'))

# Assign more readable column names.
meta <- read.delim('metadata.tsv')
colslug <- function(code) {
  slug <- meta$slug[meta$code == code]
  if (length(slug) == 0) { return(code) }
  as.character(slug)
}

colnames(ch89m) <- sapply(colnames(ch89m), colslug)
colnames(ch89dg) <- sapply(colnames(ch89dg), colslug)
colnames(ch89q) <- sapply(colnames(ch89q), colslug)
colnames(ch89pru) <- sapply(colnames(ch89pru), colslug)

save(ch89m, ch89dg, ch89q, ch89pru, file='ch89.Rdata')
