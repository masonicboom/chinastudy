#!/usr/bin/env ruby
require 'csv'

metadata = {}

primary_re = /^([A-Z][0-9]{3}) (\S+?)\s+([a-z ]+)\s(.+)$/
continuation_re = /^([A-Z][0-9]{3})\s+(.+)$/

File.open('data/CHNAME.TXT', 'r') do |f|
  f.each do |line|
    line = line.strip

    primary_match = primary_re.match(line)
    if !primary_match.nil?
      code = primary_match[1]
      slug = primary_match[2]
      type = primary_match[3]
      desc = primary_match[4]
      metadata[code] = {
        code: code,
        slug: slug,
        type: type,
        desc: desc,
      }
    else
      continuation_match = continuation_re.match(line)
      next if continuation_match.nil?
      code = continuation_match[1]
      desc = continuation_match[2]
      metadata[code][:desc] += ' ' + desc
    end
  end
end

headers = [:code, :slug, :type, :desc]
CSV.open('metadata.tsv', 'w', headers: headers, write_headers: true, col_sep: "\t") do |csv|
  metadata.values.each { |vals| csv << vals }
end
